/*
 * ======== package.xdc ========
 *
 */

/*
 * List the packages to be included in the bundle. The 'requires'
 * statements must come before the 'package' statement.
 */


/*!
 *  ======== ti.sdo.edma3.rm ========
 */
package ti.sdo.edma3.rm [01, 03, 08] {
    module RM;
}
